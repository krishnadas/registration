from django import forms
from .models import Member
from django.utils.translation import ugettext_lazy as _

class SignupForm(forms.Form):
    """
    Form for registering a new user account.
    
    Validates that the requested username is not already in use, and
    requires the password to be entered twice to catch typos.
    
    Subclasses should feel free to add any additional validation they
    need, but should avoid defining a ``save()`` method -- the actual
    saving of collected user data is delegated to the active
    registration backend.

    """
    required_css_class = 'required'

    email = forms.EmailField(widget=forms.TextInput(attrs={'class':'form-control logpadding', 'placeholder':"Email"}),
                                label=u'')
    
    def clean_email(self):
        """
        Validates that the email is not already in use.
        
        """
        if self.cleaned_data.get('email', None):
            try:
                user = Member.objects.get(email__exact=self.cleaned_data['email'])
            except Member.DoesNotExist:
                return self.cleaned_data['email']
            raise forms.ValidationError(_('Email "%s" already exists.' % self.cleaned_data['email']))

class LoginForm(forms.Form):
    """Login form.
    """
    username = forms.CharField(max_length=100, required=True,widget=forms.TextInput(attrs={'class':'form-control logpadding', 'placeholder':"Username"}),
                                label=u'')
    password = forms.CharField(widget=forms.widgets.PasswordInput(attrs={'class':'form-control logpadding', 'placeholder':"Password"}), required=True)
    def clean_username(self):
        """
        Validates that the email is not already in use.
        
        """
        if self.cleaned_data.get('username', None):
            try:
                user = Member.objects.get(username__exact=self.cleaned_data['username'])
            except Member.DoesNotExist:
                raise forms.ValidationError(_('unsername "%s" does not exists.' % self.cleaned_data['username']))
            return self.cleaned_data['username']
    def clean_password(self):
        """
        Validates that the email is not already in use.
        
        """
        if self.cleaned_data.get('password', None):
            try:
                user = authenticate(username=self.cleaned_data['username'], password=self.cleaned_data['password'])
            except:
                raise forms.ValidationError(_('username and password does not match.') )
            return self.cleaned_data['username']

class PasswordResetForm(forms.Form):
    password1 = forms.CharField(
        label=_('New password'),
        widget=forms.PasswordInput(attrs={'class':'form-control logpadding', 'placeholder':"New Password"}),
    )
    password2 = forms.CharField(
        label=_('New password (confirm)'),
        widget=forms.PasswordInput(attrs={'class':'form-control logpadding', 'placeholder':"New Password(confirm)"}),
    )

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user')
        super(PasswordResetForm, self).__init__(*args, **kwargs)

    def clean_password2(self):
        password1 = self.cleaned_data.get('password1', '')
        password2 = self.cleaned_data['password2']
        if not password1 == password2:
            raise forms.ValidationError(_("The two passwords didn't match."))
        return password2

    def save(self):
        self.user.set_password(self.cleaned_data['password1'])
        Member.objects.filter(pk=self.user.pk).update(
            password=self.user.password,
        )

class ForgotPasswordForm(forms.Form):
    """Login form.
    """
    email = forms.EmailField(widget=forms.TextInput(attrs={'id':'inputRegisterEmail'}),
                                label=u'')
    def clean_email(self):
        """
        Validates that the email is not already in use.
        
        """
        if self.cleaned_data.get('email', None):
            try:
                user = Member.objects.get(email__exact=self.cleaned_data['email'])
                return user.username
            except Member.DoesNotExist:
                raise forms.ValidationError(u'Email "%s" does not exists.' % self.cleaned_data['email'])
            return self.cleaned_data['email']