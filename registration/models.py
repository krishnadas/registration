from django.db import models
import datetime, random, sha, re

from django.utils import timezone
from portal.unique_slugify import unique_slugify
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager,PermissionsMixin
from django.template.loader import render_to_string
from django.core.mail import EmailMultiAlternatives
from django.utils.html import strip_tags
from django.conf import settings


class MemberManager(BaseUserManager):



    

    def verify_user(self, verification_key):
        """
        Given the activation key, makes a User's account active if the
        activation key is valid and has not expired.
        
        Returns the User if successful, or False if the account was
        not found or the key had expired.
        
        """
        # Make sure the key we're trying conforms to the pattern of a
        # SHA1 hash; if it doesn't, no point even trying to look it up
        # in the DB.
        if re.match('[a-f0-9]{40}', verification_key):
            try:
                maker = self.get(verification_key=verification_key)
            except self.model.DoesNotExist:
                return False
            if not maker.verification_key_expired():
                # Account exists and has a non-expired key. Activate it.
                
                maker.is_verified = True
                maker.save()
                return maker
            if maker:
                return maker
            return False









class Member(AbstractBaseUser, PermissionsMixin):

        email = models.EmailField('email address',max_length=254, unique=True)
        verification_key = models.CharField(max_length=40)
        key_generated = models.DateTimeField()
        is_verified = models.BooleanField()
        slug = models.SlugField(max_length=255, unique=True)
        objects = MemberManager()

      
        
        USERNAME_FIELD = 'email'
        REQUIRED_FIELDS = ['email','mobile']

        


        def has_perm(self, perm, obj=None):
            "Does the user have a specific permission?"
        # Simplest possible answer: Yes, always
            return True


        def has_module_perms(self, app_label):
            "Does the user have permissions to view the app `app_label`?"
        # Simplest possible answer: Yes, always
            return True


        @property
        def is_staff(self):
            "Is the user a member of staff?"
        # Simplest possible answer: All admins are staff
            return self.is_admin


        
        def save(self, *args, **kwargs):
            if not self.id:
                unique_slugify(self,self.username)
                self.key_generated = timezone.now()
                
                #e=EmployerReg_Form(username=username,email=email,password=password,usertype=usertype)
                #e.save()

                salt = sha.new(str(random.random())).hexdigest()[:5]
                verification_key = sha.new(salt+self.email).hexdigest()

                subject = "Activate your Nellikkal tours and travels account"
                
                html_content = render_to_string('registration/mail/welcome.html', { 'site_url': settings.SITE_URL, 
                                            'verification_key': verification_key,
                                            'expiration_days': settings.ACCOUNT_ACTIVATION_DAYS })
                text_content = strip_tags(html_content)
                # create the email, and attach the HTML version as well.
                msg = EmailMultiAlternatives(subject, text_content, settings.EMAIL_TO, to=[self.email])
                msg.attach_alternative(html_content, "text/html")
                msg.send()
                self.verification_key = verification_key
            super(Member, self).save(*args, **kwargs)
    
    
    
        def verification_key_expired(self):
            
            expiration_date = datetime.timedelta(days=30)
            return self.key_generated + expiration_date <= timezone.now()
