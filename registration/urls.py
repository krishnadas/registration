from .views import verify, signin,signout,resend_verification,signup,recover,recover_done,reset,reset_done
from django.conf.urls import patterns, url





urlpatterns = patterns('',
                       # Activation keys get matched by \w+ instead of the more specific
                       # [a-fA-F0-9]+ because a bad activation key should still get to the view;
                       # that way it can return a sensible "invalid key" message instead of a
                       # confusing 404.
 
                       url(r'^verify/(?P<verification_key>\w+)/$', verify, name='verify'),
                       url(r'^re-verify/$', resend_verification ,name='re-verify'),
  
                      
                       #(r'^Kitchen/$', kitchen ),
                       # url(r'^forgot/$', forgot_password ,name='forgot'),
                       url(r'^signin/$', signin ,name='signin'),
                       url(r'^signout/$', signout ,name='signout'),
                       
                       
                       url(r'^signup/$',signup, name='signup'),
                       
                       #password reset
                       url(r'^recover/(?P<signature>.+)/$', recover_done,name='password_reset_sent'),
                       url(r'^recover/$', recover, name='password_reset_recover'),
                       url(r'^reset/done/$', reset_done, name='password_reset_done'),
                       url(r'^reset/(?P<token>[\w:-]+)/$', reset,name='password_reset_reset'),
                       
    

                       #(r'^SignupComplete/$', signup_compelete),
                       
             		

              )

