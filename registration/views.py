from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.core.urlresolvers import reverse, reverse_lazy
from apps.registration import forms

from django.conf import settings
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response,get_object_or_404,redirect
from django.template import RequestContext,Context,loader
from django.template.loader import render_to_string
from django.contrib.auth.decorators import login_required

from django.core.mail import send_mail,EmailMultiAlternatives
from django.utils.html import strip_tags

from .models import Member
from .forms import SignupForm,SignInForm,PasswordResetForm,ForgotPasswordForm

import datetime, random, sha

from django.contrib.sites.models import RequestSite
from django.core import signing
from django.http import Http404
from django.utils import timezone
from django.views import generic

from .utils import get_user_model


@login_required(login_url='/accounts/signin')
def resend_verification(request):

    email = request.user.email
    salt = sha.new(str(random.random())).hexdigest()[:5]
    verification_key = sha.new(salt+email).hexdigest()
    subject = "Activate your new account at nellikkal tour and travels"
    html_content = render_to_string('registration/mail/welcome.html', { 'site_url': settings.SITE_URL, 
                                        'verification_key': verification_key,
                                        'expiration_days': settings.ACCOUNT_ACTIVATION_DAYS })
    text_content = strip_tags(html_content)
    msg = EmailMultiAlternatives(subject, text_content, settings.EMAIL_TO, to=[email])
    msg.attach_alternative(html_content, "text/html")
    msg.send()       
    member = request.user
    member.verification_key = verification_key
    member.save()
    return HttpResponseRedirect(reverse('apps.website.views.home'))


def verify(request, verification_key):

	verification_key = verification_key.lower() # Normalize before trying anything with it.
	account = Member.objects.verify_user(verification_key)
	if request.method == 'POST':
		form = PasswordResetForm(request.POST)
		if form.is_valid():
			password=request.POST.get('password1')
			account.set_password(password)
			account.save()
			title = 'Verifing your account'
			variables = RequestContext(request, {'title':title,'account': account,'expiration_days': settings.ACCOUNT_ACTIVATION_DAYS})
			return render_to_response('registration/activate.html',variables)
	form = PasswordResetForm()
	title = 'Set your password'
	variables = RequestContext(request, {'title':title,'account': account,'form': form})
	return render_to_response('registration/set_password.html',variables)

def signup(request):
    #login_required = False
    if request.user.is_authenticated():
        return HttpResponseRedirect(reverse('apps.website.views.home'))

	if request.method == 'POST':
		form = SignupForm(request.POST)
		if form.is_valid():
			email=request.POST.get('email')
			key_generated=datetime.datetime.now()
			is_verified=0
            #e=EmployerReg_Form(username=username,email=email,password=password,usertype=usertype)
            #e.save()

			salt = sha.new(str(random.random())).hexdigest()[:5]
			verification_key = sha.new(salt+email).hexdigest()

			subject = "Activate your Nellikkal tours and travels account"
            
			html_content = render_to_string('registration/mail/welcome.html', { 'site_url': settings.SITE_URL, 
                                        'verification_key': verification_key,
                                        'expiration_days': settings.ACCOUNT_ACTIVATION_DAYS })
			text_content = strip_tags(html_content)
			# create the email, and attach the HTML version as well.
			msg = EmailMultiAlternatives(subject, text_content, settings.EMAIL_TO, to=[email])
			msg.attach_alternative(html_content, "text/html")
			e=Member(email=email,verification_key=verification_key, key_generated=key_generated,is_verified=is_verified)
			e.set_unusable_password()
			e.save()
			# context = {
			# 	'site': RequestSite(self.request),
			# 	'user': self.user,
			# 	'token': signing.dumps(self.user.pk, salt=self.salt),
			# 	'secure': self.request.is_secure(),
			# }
			# body = loader.render_to_string('registration/recovery_email.txt',
			  #                                      context).strip()
			  #       subject = loader.render_to_string('registration/recovery_email_subject.txt',
			  #                                         context).strip()
			  #       send_mail(subject, body, settings.EMAIL_TO,
			  #                 [self.user.email])
			msg.send()
			return HttpResponseRedirect('/accounts/signin/')
    else:
        form = SignupForm()
    title = 'Free signup'
    variables = RequestContext(request, {'title':title,'form':form})

    return render_to_response('registration/signup.html',variables)



@login_required(login_url='/accounts/signin')
def signout(request):
    logout(request)
    return HttpResponseRedirect('/accounts/signin/')

   

def signin(request):
    """Login handler.
    """
    if request.user.is_authenticated():
        return HttpResponseRedirect(reverse('apps.website.views.home'))
    
    if request.method == 'GET':
        form = forms.LoginForm()
    else:
        form = forms.LoginForm(request.POST)
        if form.is_valid():
            username = request.POST['username']
            password = request.POST['password']
            user = authenticate(username=username, password=password)
            if user is not None:
                if user.is_active:
                    login(request, user)
                    if 'next' in request.GET:
                        return HttpResponseRedirect(request.GET['next'])
                    DEFAULT_URL = reverse('home')
                    return HttpResponseRedirect(request.META.get('HTTP_REFERER', DEFAULT_URL))
                    
                else:
                    messages.add_message(request, messages.ERROR, "Account disabled.")
            else:
                messages.add_message(request, messages.ERROR, "Login failed.")
    title = 'Login'
    variables = RequestContext(request, {'title':title,'form':form})
        
    return render_to_response('registration/signin.html',variables)

class SaltMixin(object):
    salt = 'password_recovery'
    url_salt = 'password_recovery_url'


def loads_with_timestamp(value, salt):
    """Returns the unsigned value along with its timestamp, the time when it
    got dumped."""
    try:
        signing.loads(value, salt=salt, max_age=-1)
    except signing.SignatureExpired as e:
        age = float(str(e).split('Signature age ')[1].split(' >')[0])
        timestamp = timezone.now() - datetime.timedelta(seconds=age)
        return timestamp, signing.loads(value, salt=salt)


class RecoverDone(SaltMixin, generic.TemplateView):
    template_name = "registration/reset_sent.html"

    def get_context_data(self, **kwargs):
        ctx = super(RecoverDone, self).get_context_data(**kwargs)
        try:
            ctx['timestamp'], ctx['email'] = loads_with_timestamp(
                self.kwargs['signature'], salt=self.url_salt,
            )
        except signing.BadSignature:
            raise Http404
        return ctx
recover_done = RecoverDone.as_view()


class Recover(SaltMixin, generic.FormView):
    case_sensitive = True
    form_class = ForgotPasswordForm
    template_name = 'registration/recovery_form.html'
    email_template_name = 'registration/email/recovery_email.txt'
    email_subject_template_name = 'registration/email/recovery_email_subject.txt'
    search_fields = ['email']

    def get_success_url(self):
        return reverse('password_reset_sent', args=[self.mail_signature])

    def get_context_data(self, **kwargs):
        kwargs['url'] = self.request.get_full_path()
        return super(Recover, self).get_context_data(**kwargs)

    def get_form_kwargs(self):
        kwargs = super(Recover, self).get_form_kwargs()
        kwargs.update({
            'case_sensitive': self.case_sensitive,
            'search_fields': self.search_fields,
        })
        return kwargs

    def send_notification(self):
        context = {
            'site': RequestSite(self.request),
            'user': self.user,
            'token': signing.dumps(self.user.pk, salt=self.salt),
            'secure': self.request.is_secure(),
        }
        body = loader.render_to_string(self.email_template_name,
                                       context).strip()
        subject = loader.render_to_string(self.email_subject_template_name,
                                          context).strip()
        send_mail(subject, body, settings.EMAIL_TO,
                  [self.user.email])

    def form_valid(self, form):
        self.user = form.cleaned_data['user']
        self.send_notification()
        if (
            len(self.search_fields) == 1 and
            self.search_fields[0] == 'username'
        ):
            # if we only search by username, don't disclose the user email
            # since it may now be public information.
            email = self.user.username
        else:
            email = self.user.email
        self.mail_signature = signing.dumps(email, salt=self.url_salt)
        return super(Recover, self).form_valid(form)
recover = Recover.as_view()


class Reset(SaltMixin, generic.FormView):
    form_class = PasswordResetForm
    token_expires = 3600 * 48  # Two days
    template_name = 'registration/reset.html'
    success_url = reverse_lazy('password_reset_done')

    def dispatch(self, request, *args, **kwargs):
        self.request = request
        self.args = args
        self.kwargs = kwargs

        try:
            pk = signing.loads(kwargs['token'], max_age=self.token_expires,
                               salt=self.salt)
        except signing.BadSignature:
            return self.invalid()

        self.user = get_object_or_404(get_user_model(), pk=pk)
        return super(Reset, self).dispatch(request, *args, **kwargs)

    def invalid(self):
        return self.render_to_response(self.get_context_data(invalid=True))

    def get_form_kwargs(self):
        kwargs = super(Reset, self).get_form_kwargs()
        kwargs['user'] = self.user
        return kwargs

    def get_context_data(self, **kwargs):
        ctx = super(Reset, self).get_context_data(**kwargs)
        if 'invalid' not in ctx:
            ctx.update({
                'username': self.user.username,
                'token': self.kwargs['token'],
            })
        return ctx

    def form_valid(self, form):
        form.save()
        return redirect(self.get_success_url())
reset = Reset.as_view()


class ResetDone(generic.TemplateView):
    template_name = 'registration/recovery_done.html'


reset_done = ResetDone.as_view()